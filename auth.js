const jwt = require("jsonwebtoken");
const secret = "Capstone2Project";

// TOKEN CREATION
module.exports.createToken = (user) => {
  const payload = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin,
  };
  return jwt.sign(payload, secret, {});
};

// TOKEN VERIFICATION
module.exports.verify = (request, response, next) => {
  let token = request.headers.authorization;
  console.log(token);

  if (token !== undefined) {
    token = token.slice(7, token.length);
    return jwt.verify(token, secret, (error, data) => {
      if (error) {
        return response.send(`Invalid token.`);
      } else {
        next();
      }
    });
  } else {
    return response.send(`Authentication failed! No token provided`);
  }
};

// TOKEN DECRYPTION
module.exports.decode = (token) => {
  if (token === undefined) {
    return null;
  } else {
    token = token.slice(7, token.length);

    return jwt.verify(token, secret, (error, data) => {
      if (error) {
        return null;
      } else {
        return jwt.decode(token, { complete: true }).payload;
      }
    });
  }
};
