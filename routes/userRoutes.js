const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../auth");

// CHECK EMAIL EXIST ROUTE
router.post("/checkEmail", userController.checkEmailExists);

// REGISTRATION ROUTE
router.post(
  "/register",
  userController.checkEmailExists,
  userController.registerUser
);

// GET ALL USERS ROUTE
router.get("/allUsers", userController.getAllUsers);

// LOGIN/AUTHENTICATION ROUTE
router.post("/login", userController.loginUser);

// UPDATE ROLE ROUTE
router.patch("/updateRole/:userId", auth.verify, userController.updateRole);

// ADD TO CART ROUTE
router.patch("/cart/:productId", auth.verify, userController.addToCart);

// CHECKOUT ROUTE
router.patch(
  "/checkout/:productId",
  auth.verify,
  userController.checkoutDirectly
);

// EXPORT ROUTER
module.exports = router;
