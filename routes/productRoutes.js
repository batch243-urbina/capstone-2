const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers");
const auth = require("../auth");

// ADD A NEW PRODUCT (ADMIN ONLY)
router.post("/addProduct", auth.verify, productController.addProduct);

// RETRIEVE ACTIVE PRODUCTS (ALL USERS ADMIN)
router.get("/activeProducts", productController.getActiveProducts);

// RETRIEVE ALL PRODUCTS
router.get("/allProducts", auth.verify, productController.getAllProducts);

// RETRIEVE SPECIFIC PRODUCT
router.get("/:productId", productController.getSpecificProduct);

// UPDATE PRODUCT
router.put(
  "/updateProduct/:productId",
  auth.verify,
  productController.updateProduct
);

// ARCHIVE A PRODUCT
router.patch(
  "/archiveProduct/:productId",
  auth.verify,
  productController.archiveProduct
);

module.exports = router;
