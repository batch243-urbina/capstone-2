const mongoose = require("mongoose");
const userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: [true, "Email is required"],
  },
  password: {
    type: String,
    required: [true, "Password is required"],
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },
  mobileNo: {
    type: String,
    required: [true, "Mobile No is required"],
  },
  orders: [
    {
      products: [
        {
          productName: {
            type: String,
          },
          productQuantity: {
            quantity: Number,
          },
        },
      ],
      totalAmount: {
        type: Number,
        default: 0,
      },
      purchasedOn: {
        type: Date,
        default: new Date(),
      },
    },
  ],
  cart: [
    {
      productId: {
        type: String,
        required: [true, "Product ID is required"],
      },
      quantity: {
        type: Number,
      },
    },
  ],
});

module.exports = mongoose.model("User", userSchema);
