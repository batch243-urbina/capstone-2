const Product = require("../models/Product");
const auth = require("../auth");

// ADD NEW PRODUCT TO DATABASE (ONLY ADMIN CAN ADD PRODUCT TO DATABASE)
module.exports.addProduct = (request, response) => {
  const userData = auth.decode(request.headers.authorization);

  let newProduct = new Product({
    name: request.body.name,
    description: request.body.description,
    price: request.body.price,
    quantity: request.body.quantity,
  });

  if (userData.isAdmin) {
    return newProduct
      .save()
      .then((product) => {
        response.send(`Product added.`);
      })
      .catch((error) => {
        response.send(`Adding a product failed. Please try again.`);
      });
  } else {
    return response.send(`You must be an admin to add a new product.`);
  }
};

// RETRIEVE ACTIVE PRODUCTS (ALL USERS AND ADMIN)
module.exports.getActiveProducts = (request, response) => {
  return Product.find({
    isActive: true,
  })
    .then((active) => {
      response.send(active);
    })
    .catch((error) => {
      response.send(error);
    });
};

// RETRIEVE SPECIFIC PRODUCT (ALL USERS AND ADMIN)
module.exports.getSpecificProduct = (request, response) => {
  const productId = request.params.productId;

  return Product.findById(productId)
    .then((product) => {
      response.send(product);
    })
    .catch((error) => {
      response.send(error);
    });
};

// RETRIEVE ALL PRODUCTS (ADMIN ONLY)
module.exports.getAllProducts = (request, response) => {
  const userData = auth.decode(request.headers.authorization);

  if (userData.isAdmin) {
    return Product.find({})
      .then((allProduct) => {
        console.log(allProduct);
        response.send(allProduct);
      })
      .catch((error) => {
        response.send(error);
      });
  } else {
    return response.send(`Sorry, you do not have access to this page.`);
  }
};

// UPDATE A PRODUCT (ADMIN)
module.exports.updateProduct = (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  const productId = request.params.productId;

  let updatedProduct = {
    name: request.body.name,
    description: request.body.description,
    price: request.body.price,
    quantity: request.body.quantity,
  };

  if (userData.isAdmin) {
    return Product.findByIdAndUpdate(productId, updatedProduct, { new: true })
      .then((updatedProduct) => {
        response.send(updatedProduct);
      })
      .catch((error) => {
        response.send(error);
      });
  } else {
    return response.send(`You must be an admin to update a product.`);
  }
};

// ARCHIVE A PRODUCT
module.exports.archiveProduct = (request, response) => {
  let userData = auth.decode(request.headers.authorization);
  let productId = request.params.productId;

  if (userData.isAdmin) {
    return Product.findById(productId)
      .then((archivedProduct) => {
        console.log(archivedProduct);
        let archivedId = { isActive: !archivedProduct.isActive };

        return Product.findByIdAndUpdate(productId, archivedId, { new: true })
          .then((productDocument) => {
            response.send(`Product's status has been updated`);
          })
          .catch((error) => {
            response.send(error);
          });
      })
      .catch((error) => {
        response.send(error);
      });
  }
};
